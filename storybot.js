var StoryBot = function(){

	//options
		//(BOOL) verbose: log results to console 
		//(function) defaultErrorResponse: what to say, by default, if command wasn't recognized
		//(number) timeToRespond: when no answer is given, revert back to root

	var currentAnswer,
		stories,
		talkCallback,
		defaultErrorResponse,
		timeToRespond,
		timeToRespondRef,
		defaultResponses;

	this.init = function(options){
		
		stories 				= options.stories || stories;
		talkCallback 			= options.talkCallback || false;
		verbose 				= options.verbose || false;
		defaultErrorResponse 	= options.defaultErrorResponse || false;
		timeToRespond 			= options.timeToRespond || 15000; //default is 15s to respond
		defaultResponses		= options.defaultResponses || [];

		//init with the root object
		setStoryToRoot();

	}

	var setStoryToRoot = function(){
		consoleLog(">>reset story to root")
		currentAnswer = stories;
	}

	//error Logging. Switchable because can get messy
	var consoleLog = function(msg){
		if(verbose)	console.log(msg);
	}
	this.toggleVerbose = function(verboseSetting){
		verbose = verboseSetting; 
	}
	
	//create response
	this.respond = function(words){
		
		//with each respons, the user is given a nr. of seconds to respond again
		clearTimeout(timeToRespondRef);
		timeToRespondRef = setTimeout(setStoryToRoot,timeToRespond);

		//start to search
		consoleLog("searching for: "+words);
		
		//find the word in triggers
		var answer = findWordsInTriggers(words, currentAnswer);
		
		//if an answer was found:
		if(answer){
			
			//say the response
			consoleLog("answer: " +  answer.response); 
			if(talkCallback) talkCallback(answer.response);

			if(answer.jumpTo){
				currentAnswer = findLabel(answer.jumpTo);
				return;
			}
			//check if there is something new to expect
			if(typeof answer.answers == "undefined"){
				//no more answers downstream, so back to root
				consoleLog("<<end of tree, return to root>>");
				setStoryToRoot();
			} else {
				//yes, so currentAnswer set is reduced to smaller set.
				currentAnswer = answer.answers;
			}
		
		//words were not recognized in the main story thread
		} else {
			
			consoleLog("could not find words");

			//if we didn't find a hit with regular answers, 
			//we check if there\s another line that is interesting.

			if(defaultResponses.length > 0){
				var answer = findWordsInTriggers(words, defaultResponses);
				if(answer && talkCallback) talkCallback(answer.response);
				return;
			}
			
			if(talkCallback && currentAnswer.errorResponse){
				talkCallback(currentAnswer.errorResponse);
				return;
			};

			if(talkCallback && defaultErrorResponse){
				talkCallback(defaultErrorResponse);
			}


		}
	}

	var findLabel = function(label){
		//here we are going to find a label of some sort by traversing through the whole storytree
		var foundAnswerForLabel;

		var recursiveSearch = function(label, answers){
			_.each(answers, function(answer){
				if(foundAnswerForLabel) return; //stop traversing
				if(answer.label == label){
					foundAnswerForLabel = answer;
				}
				if(answer.answers) recursiveSearch(label, answer.answers);
			})
		}
		
		recursiveSearch(label, stories);
		
		currentAnswer = foundAnswerForLabel;
		if(talkCallback) talkCallback(foundAnswerForLabel.response);
	}

	var findWordsInTriggers = function(words, answers){
		var matchedAnswer;
		
		//first split up the words
		var wordsArr = words.split(" ");

		if(wordsArr.length < 1) return;
		
		wordsArr = _.filter(wordsArr,function(word){
			return word.length >= 2 && /^[0-9a-zA-Z]+$/.test(word);
		});

		consoleLog("these are the words we're looking for: "+ wordsArr.join(", "));
		
		//then loop through the words
		_.each(wordsArr, function(word){
			var _word = word.toLowerCase();
			_.each(answers,function(answer, key){
				consoleLog("\tlooking for "+_word+" in "+answer.triggers.join(", "));
				var index = answer.triggers.indexOf(_word);
				if(index >= 0){
					consoleLog(">>> found word in answer!");
					matchedAnswer = (typeof matchedAnswer == "undefined") ? answer : matchedAnswer;
				}
			})
		})
		return matchedAnswer;
	}

}